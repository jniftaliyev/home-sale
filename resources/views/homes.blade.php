@if (!empty($homes))
    @foreach ($homes as $home)
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $home->name }}</td>
            <td>{{ $home->bedrooms }}</td>
            <td>{{ $home->bathrooms }}</td>
            <td>{{ $home->storeys }}</td>
            <td>{{ $home->garages }}</td>
            <td>{{ $home->price }}</td>
        </tr>
    @endforeach
@endif
