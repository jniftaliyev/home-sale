<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Sales</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body class="">

    <div class="row">
        <div class="col-3 border p-5">
            <div class="card-header">
                <h3>Search</h3>
            </div>
            <div class="form p-2">
                <label for="">Name</label>
                <input type="text" class="form-control search" id="name">
            </div>
            <div class="form p-2">
                <label for="">Bedrooms</label>
                <input type="text" class="form-control search" id="bedroom">
            </div>
            <div class="form p-2">
                <label for="">Bathrooms</label>
                <input type="text" class="form-control search" id="bathroom">
            </div>
            <div class="form p-2">
                <label for="">Storeys</label>
                <input type="text" class="form-control search" id="storey">
            </div>
            <div class="form p-2">
                <label for="">Garages</label>
                <input type="text" class="form-control search" id="garage">
            </div>
            <div class="form row p-2">
                <label for="">Price</label>
                <div class="col-6">
                    <input type="number" class="form-control search" id="minPrice">
                </div>
                <div class="col-6">
                    <input type="number" class="form-control search" id="maxPrice">
                </div>

            </div>

        </div>
        <div class="col-9 p-5">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Number</th>
                        <th scope="col">Home name</th>
                        <th scope="col">Bedrooms</th>
                        <th scope="col">Bathrooms</th>
                        <th scope="col">Storeys</th>
                        <th scope="col">Garages</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody id="homes_table">
                    {{-- HOMES --}}
                </tbody>
            </table>
            @if (isset($homes))
                {{ $homes->links() }}
            @endif
        </div>
    </div> ̰
</body>

</html>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        let url = "{{ route('homes.list') }}";
        $.get({
            url: url,
            success: function(resp) {
                $("#homes_table").html(resp);
            },
            error: function(err) {

            }
        })
    })

    $(document).on("keyup", ".search", function() {
        let url = "{{ route('homes.list') }}";
        let data = {
            name: $("#name").val(),
            bedrooms: $("#bedroom").val(),
            bathrooms: $("#bathroom").val(),
            storeys: $("#storey").val(),
            garages: $("#garage").val(),
            minPrice: $("#minPrice").val(),
            maxPrice: $("#maxPrice").val(),
        }
        console.log(url);
        $.get({
            url: url,
            // dataType: 'json',
            data: data,
            success: function(resp) {
                $("#homes_table").html(resp);
                // console.log(resp);
            },
            error: function(err) {
                console.log(err)
            }
        })
    })
</script>
