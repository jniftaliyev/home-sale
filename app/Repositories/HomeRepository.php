<?php

namespace App\Repositories;

use App\Models\Home;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HomeRepository{

    public function selectAll($data){

        try {

            $query = Home::where('status', 1);

            if(isset($data['name'])){
                $query->where('name', 'like', '%'.$data['name'].'%');
            }
            if(isset($data['bedrooms'])){
                $query->where('bedrooms', $data['bedrooms']);
            }
            if(isset($data['bathrooms'])){
                $query->where('bathrooms', $data['bathrooms']);
            }
            if(isset($data['storeys'])){
                $query->where('storeys', $data['storeys']);
            }
            if(isset($data['garages'])){
                $query->where('garages', $data['garages']);
            }
            if(isset($data['minPrice'])){
                $query->where('price','>=', $data['minPrice']);
            }
            if(isset($data['maxPrice'])){
                $query->where('price','<=', $data['maxPrice']);
            }

            $homes = $query->paginate(3);
            if ($homes == null)
                throw new ModelNotFoundException("Any home not found");

            return $homes;

        } catch (\Throwable $th) {
                throw $th;
        }
    }

}

?>
