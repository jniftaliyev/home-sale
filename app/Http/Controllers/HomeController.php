<?php

namespace App\Http\Controllers;

use App\Http\Resources\HomeResource;
use App\Models\Home;
use App\Repositories\HomeRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $homeRepository;

     public function __construct(HomeRepository $homeRepository)
     {
        $this->homeRepository = $homeRepository;
     }

    public function index(Request $request)
    {

        return view('index');
    }

    public function list(Request $request){
        $data = $request->all();
        $homes = $this->homeRepository->selectAll($data);

        // return HomeResource::collection($homes);

        return view('homes', ['homes' => $homes]);
    }


}
