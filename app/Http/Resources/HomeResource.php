<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          'name'        =>  $this->name,
          'bedrooms'    =>  $this->bedrooms,
          'bathrooms'   =>  $this->bathrooms,
          'storeys'     =>  $this->storeys,
          'garages'     =>  $this->garages,
          'price'       =>  $this->price,
        ];
    }
}
